package amoeba.thermocup.services;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Handler;

import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.UUID;

import amoeba.thermocup.MainActivity;

/**
 * Created by liisi on 10/31/15.
 */
public class BluetoothService {

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothSocket mmSocket;
    private BluetoothDevice mmDevice;
    //    OutputStream mmOutputStream;
    private InputStream mmInputStream;
    private Thread workerThread;
    private byte[] readBuffer;
    private int readBufferPosition;
    private volatile boolean stopWorker;

    private MainActivity activity;
    private String deviceName;

    public BluetoothService(MainActivity activity) {
        this.activity = activity;
        this.deviceName = "termocup";
    }

    public void findBT() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            activity.setMessage("No bluetooth adapter available");
        }

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(enableBluetooth, 0);
        }

        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (device.getName().equals(this.deviceName)) {
                    mmDevice = device;
                    activity.setMessage("Device " + this.deviceName + " (" + device.getAddress() + ") is paired");
                    break;
                }
            }
        }
        if (mmDevice == null) {
            activity.setMessage("Device " + this.deviceName + " not found, please pair the devices first.");
        }
    }

    public void openBT() throws IOException {

        initConnection();
        beginListenForData();

        activity.setMessage("Bluetooth connection opened");
    }

    private void initConnection() throws IOException {
        if (mmDevice == null) {
            activity.setMessage("No device found");
            return;
        }

        UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"); //Standard SerialPortService ID
        mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
        mmSocket.connect();
//        mmOutputStream = mmSocket.getOutputStream();
        mmInputStream = mmSocket.getInputStream();
    }

    void beginListenForData() {
        final Handler handler = new Handler();
        final byte delimiter = 10; //This is the ASCII code for a newline character

        stopWorker = false;
        readBufferPosition = 0;
        readBuffer = new byte[1024];
        workerThread = new Thread(new Runnable() {
            public void run() {
                long lastConnectionTime = getTimeStamp();
                long currentTime;
                int maxIdleWaitingTime = 300;
                while (!Thread.currentThread().isInterrupted() && !stopWorker) {
                    try {
                        if (mmSocket.isConnected()) {
                            int bytesAvailable = mmInputStream.available();
                            if (bytesAvailable > 0) {
                                //activity.setMessage("Data found");
                                byte[] packetBytes = new byte[bytesAvailable];
                                mmInputStream.read(packetBytes);
                                for (int i = 0; i < bytesAvailable; i++) {
                                    byte b = packetBytes[i];
                                    if (b == delimiter) {
                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                        final String data = new String(encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;

                                        handler.post(new Runnable() {
                                            public void run() {
                                                activity.setTemperatureLabel(data);
                                            }
                                        });
                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }
                            // set/update last connected time
                            lastConnectionTime = getTimeStamp();
                        } else {
                            // wait x ms
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                                // nothing happens if sleep is interrupted, we just continue execution
                            }
                            // try to reconnect
                            initConnection();
                        }

                        // if no connection in x ms, stop (stopWorker)
                        currentTime = getTimeStamp();
                        if (currentTime > lastConnectionTime + maxIdleWaitingTime) {
                            stopWorker = true;
                        }
                    } catch (IOException ex) {
                        stopWorker = true;
                    }
                }
            }

            private long getTimeStamp() {
                return System.currentTimeMillis() / 1000;
            }
        });

        workerThread.start();
    }

    public void closeBT() throws IOException {
        stopWorker = true;
//        mmOutputStream.close();
        if (mmInputStream != null) {
            mmInputStream.close();
        }
        if (mmSocket != null) {
            mmSocket.close();
        }
        activity.setMessage("Bluetooth Closed");
    }
}
