package amoeba.thermocup;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import amoeba.thermocupapp.R;
import amoeba.thermocup.services.BluetoothService;


public class MainActivity extends ActionBarActivity {

    private TextView tempLabel;
    private TextView messages;
    private TextView logs;

    private BluetoothService service;

    public void setMessage(String message) {
        //this.messages.setText(message);
        log(message);
    }

    public void setTemperatureLabel(String temp) {
        double parsedTemp;
        int textColor = getResources().getColor(R.color.red);

        try {
            parsedTemp = Double.parseDouble(temp);
            if (parsedTemp >= 60) {
                textColor = getResources().getColor(R.color.red);
            } else if (parsedTemp >= 50) {
                textColor = getResources().getColor(R.color.yellow);
            } else if (parsedTemp >= 30) {
                textColor = getResources().getColor(R.color.green);
            } else {
                textColor = getResources().getColor(R.color.blue);
            }
        } catch (NumberFormatException e) {
            // log("Temperature " + temp + " not a number");
        }

        this.tempLabel.setTextColor(textColor);
        this.tempLabel.setText(temp);
    }

    private void log(String message) {
        String formattedTime = new SimpleDateFormat("HH:mm:ss").format(new Date());
        this.logs.setText(String.format("%s %s\n%s", formattedTime, message, this.logs.getText()));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button findButton = (Button) findViewById(R.id.find);
        Button openButton = (Button) findViewById(R.id.open);
        Button closeButton = (Button) findViewById(R.id.close);
        tempLabel = (TextView) findViewById(R.id.tempLabel);
//        messages = (TextView) findViewById(R.id.messages);
        logs = (TextView) findViewById(R.id.logs);

        service = new BluetoothService(this);

        //Find Button
        findButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                service.findBT();
            }
        });

        //Open Button
        openButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    service.openBT();
                } catch (IOException ex) {
                }
            }
        });

        //Close button
        closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    service.closeBT();
                } catch (IOException ex) {
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
